package io.github.drp19.schoolbot;

import discord4j.core.DiscordClient;
import discord4j.core.DiscordClientBuilder;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.channel.PinsUpdateEvent;
import discord4j.core.event.domain.guild.GuildCreateEvent;
import discord4j.core.event.domain.guild.MemberJoinEvent;
import discord4j.core.event.domain.lifecycle.ReadyEvent;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.event.domain.message.ReactionAddEvent;
import discord4j.core.event.domain.message.ReactionRemoveEvent;
import discord4j.core.object.entity.Guild;
import discord4j.core.object.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class SchoolBot {
	final static private Logger logger = LoggerFactory.getLogger(SchoolBot.class);

	public static void main(String[] args) {
		if (args.length != 1) {
			System.err.println("Must have one parameter with the bot token.");
			return;
		}
		final DiscordClient client = DiscordClient.create(args[0]);
		final GatewayDiscordClient gateway = client.login().block();
		gateway.on(ReadyEvent.class)
				.subscribe(event -> {
					User self = event.getSelf();
					logger.info(String.format("Logged in as %s#%s", self.getUsername(), self.getDiscriminator()));
					System.out.println("\n\n\n\n\n\nUse the following link to invite the bot to your server:");
					System.out.println("https://discordapp.com/oauth2/authorize?client_id=" + gateway.getSelfId().asString() + "&scope=bot&permissions=402721861");
					System.out.println("If you don't allow all the perms, the bot probably won't work right.");
					System.out.println("Use with /voteban @user {reason}\n\n\n\n\n");
				});
		registerClassRole(gateway);

		gateway.onDisconnect().block();
	}

	private static void registerClassRole(GatewayDiscordClient gateway) {
		gateway.on(MessageCreateEvent.class).subscribe(ClassRoleAction::processMessageEvent);
		gateway.on(ReactionAddEvent.class).subscribe(ClassRoleAction::processReactionAddEvent);
		gateway.on(GuildCreateEvent.class).subscribe(ClassRoleAction::processGuildEvent);
		gateway.on(ReactionRemoveEvent.class).subscribe(ClassRoleAction::processReactionRemoveEvent);
		gateway.on(PinsUpdateEvent.class).subscribe(ClassRoleAction::processPinUpdate);
	}
}
