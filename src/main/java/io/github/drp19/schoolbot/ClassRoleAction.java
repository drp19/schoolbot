package io.github.drp19.schoolbot;

import discord4j.common.util.Snowflake;
import discord4j.core.event.domain.channel.PinsUpdateEvent;
import discord4j.core.event.domain.guild.GuildCreateEvent;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.event.domain.message.ReactionAddEvent;
import discord4j.core.event.domain.message.ReactionRemoveEvent;
import discord4j.core.object.PermissionOverwrite;
import discord4j.core.object.entity.*;
import discord4j.core.object.entity.channel.Category;
import discord4j.core.object.entity.channel.Channel;
import discord4j.core.object.entity.channel.GuildChannel;
import discord4j.core.object.entity.channel.TextChannel;
import discord4j.core.object.reaction.ReactionEmoji;
import discord4j.rest.util.Permission;
import discord4j.rest.util.PermissionSet;
import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class ClassRoleAction {
	private static final Logger logger = LoggerFactory.getLogger(ClassRoleAction.class);
	private static final ReactionEmoji ADD_EMOJI = ReactionEmoji.unicode("\u270C\uFE0F");
	private static final ReactionEmoji DONE_EMOJI = ReactionEmoji.unicode("\u2705");
	private static final ReactionEmoji WARNING_EMOJI = ReactionEmoji.unicode("\u26a0");

	private static final Map<String, ClassRoleAction> servers = new HashMap<>();

	private static class ChannelListing {
		private final Snowflake id;
		private final boolean channel, role;
		private final int reacts;
		private final String regex;
		@Nullable private final Snowflake category;

		private ChannelListing(@NonNull Snowflake id, boolean role, boolean channel, int reacts, @NonNull String regex, @Nullable Snowflake category) {
			this.id = id;
			this.channel = channel;
			this.role = role;
			this.reacts = reacts;
			this.regex = regex;
			this.category = category;
		}

		private boolean matches(@NonNull String message) {
			return message.matches(regex);
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;
			ChannelListing that = (ChannelListing) o;
			return Objects.equals(id, that.id);
		}

		@Override
		public int hashCode() {
			return Objects.hash(id);
		}
	}

	private final List<ChannelListing> channels;

	private final Guild guild;

	private ClassRoleAction (@NonNull Guild guild, @NonNull List<ChannelListing> channels) {
		this.guild = guild;
		this.channels = channels;
	}

	/*
	If message in channel does not match the correct format, delete it.
	- If the channel exists but not role, immediately create a role and modify the channel permissions and give author the role.
	- If the role exists but not the channel, immediately create the channel and give the author the role.
	- If both exist, delete the message.
	- If neither exist, do the normal vote process.
	 */
	private void processMessage(@NonNull Message message, ChannelListing channelListing) {
		String messageContent = message.getContent().toLowerCase();
		if (!channelListing.matches(messageContent)) {
			logger.info("Does not match");
			if (!message.getAuthorAsMember().block().getId().asString().equals("213118265633800192"))
				message.delete().block();
			return;
		}
		Channel channel = findChannelByName(messageContent);
		Role role = findRoleByName(messageContent);
		boolean roleExists = role != null;
		boolean channelExists =  channel != null;
		if (roleExists && channelExists) {
			message.delete().block();
		} else if (!roleExists && !channelExists) {
			message.addReaction(ADD_EMOJI).block();
		} else {
			message.addReaction(ADD_EMOJI).block();
			message.addReaction(DONE_EMOJI).block();
			if (channelExists && channelListing.role) {
				role = createRole(messageContent);
			} else if (roleExists && channelListing.channel) {
				channel = createChannel(messageContent, channelListing.category);
			} else if (roleExists && channelListing.role) {
				message.addReaction(WARNING_EMOJI).block();
			} else {
				message.delete().block();
			}
			if (channelListing.role && channelListing.channel && channel != null && role != null) {
				modifyChannelPermissions(channel, role);
			}
			//message.getAuthorAsMember().block().addRole(role.getId()).block();
		}
	}

	/*
	If react is the correct emoji
	- If the bot has not done the things and there are not enough reacts, do nothing.
	- If there are enough reacts, create a new role, a new channel, and add all users who reacted to the role.
	- If the bot has done the things, add the user to the role.
	 */
	private void processAddReaction(@NonNull Message message, @NonNull Member member, @NonNull ReactionEmoji emoji, ChannelListing channel) {
		if (!emoji.equals(ADD_EMOJI)) {
			return;
		}
		String messageContent = message.getContent().toLowerCase();
		if (message.getReactors(DONE_EMOJI).filter(user -> user.getId().equals(guild.getClient().getSelfId())).blockFirst() == null) {
			// has not passed yet
			Long reactions = message.getReactors(ADD_EMOJI).count().block();
			if (reactions != null && reactions >= channel.reacts) {
				//enough reactions
				Channel existingChannel = findChannelByName(messageContent);
				Role existingRole = findRoleByName(messageContent);
				if (existingRole == null && channel.role) {
					existingRole = createRole(messageContent);
				}
				if (findChannelByName(messageContent) == null && channel.channel) {
					existingChannel = createChannel(messageContent, channel.category);
				}
				if (channel.channel && channel.role && existingChannel != null && existingRole != null) {
					modifyChannelPermissions(existingChannel, existingRole);
				}
				message.addReaction(DONE_EMOJI).block();
				if (channel.role) {
					for (User user : message.getReactors(ADD_EMOJI).toIterable()) {
						Member reactMember = guild.getMemberById(user.getId()).block();
						if (reactMember == null) {
							logger.warn("Reaction user is not a member");
							return;
						}
						if (existingRole != null) {
							reactMember.addRole(existingRole.getId()).block();
						}
					}
				}
			}
		} else {
			// has passed
			if (channel.role) {
				Role toGive = findRoleByName(messageContent);
				if (toGive == null) {
					logger.warn("Could not find role {} to assign", messageContent);
					return;
				}
				logger.info("Adding role for user {}", member.getId().asString());
				member.addRole(toGive.getId()).block();
			}
		}
	}

	/*
	If user removes the correct emoji from a message in the right channel,
	AND the bot has already reacted done to it,
	remove the user's role.
	 */
	private void processRemoveReaction(@NonNull Message message, @NonNull User user, @NonNull ReactionEmoji emoji, ChannelListing channel) {
		if (!emoji.equals(ADD_EMOJI)) {
			return;
		}
		String messageContent = message.getContent().toLowerCase();
		if (channel.role && message.getReactors(DONE_EMOJI).filter(reactUser -> reactUser.getId().equals(guild.getClient().getSelfId())).blockFirst() != null) {
			// has passed
			Role toGive = findRoleByName(messageContent);
			if (toGive == null) {
				logger.warn("Could not find role {} to remove", messageContent);
				return;
			}
			Member member = user.asMember(guild.getId()).block();
			if (member == null) {
				logger.warn("User is not a member???");
				return;
			}
			logger.info("Removing role for user {}", member.getId().asString());
			member.removeRole(toGive.getId()).block();
		}
	}

	@Nullable
	private Channel createChannel(String name, Snowflake categoryId) {
		logger.info("Creating channel with name {}}", name);
		Channel channel = guild.createTextChannel(textChannelCreateSpec -> {
			textChannelCreateSpec.setName(name);
			if (categoryId != null) {
				textChannelCreateSpec.setParentId(categoryId);
			}
		}).block();
		if (channel == null) {
			logger.warn("Failed to create channel");
			return null;
		}
		//modifyChannelPermissions(channel, channelRole);
		return channel;
	}

	private void modifyChannelPermissions(Channel channel, Role role) {
		PermissionOverwrite allowRoleOverwrite = PermissionOverwrite.forRole(role.getId(), PermissionSet.of(Permission.SEND_MESSAGES, Permission.VIEW_CHANNEL), PermissionSet.none());
		PermissionOverwrite everyoneOverwrite = PermissionOverwrite.forRole(guild.getEveryoneRole().block().getId(), PermissionSet.none(), PermissionSet.of(Permission.SEND_MESSAGES, Permission.VIEW_CHANNEL));

		if (channel.getType().equals(Channel.Type.GUILD_TEXT)) {
			logger.info("Adding permissions to channel {}", channel.getId().asString());
			((TextChannel) channel).edit(textChannelEditSpec -> {
				textChannelEditSpec.setPermissionOverwrites(Set.of(allowRoleOverwrite, everyoneOverwrite));
			}).block();
		}
	}

	@Nullable
	private Role createRole(String roleName) {
		logger.info("Creating role with name {}", roleName);
		return guild.createRole(roleCreateSpec -> {
			roleCreateSpec.setName(roleName);
			roleCreateSpec.setPermissions(PermissionSet.none());
		}).block();
	}

	@Nullable
	private Channel findChannelByName(String name) {
		return guild.getChannels().filter(guildChannel -> guildChannel.getName().equalsIgnoreCase(name)).blockFirst();
	}

	@Nullable
	private Role findRoleByName(String name) {
		return guild.getRoles().filter(role -> role.getName().equalsIgnoreCase(name)).blockFirst();
	}

	@Nullable
	private ChannelListing getChannelListing(Snowflake id) {
		return channels.stream().filter(channelListing -> channelListing.id.equals(id)).findFirst().orElse(null);
	}

	public static void processMessageEvent(MessageCreateEvent event) {
		Guild guildObject = event.getGuild().block();
		if (guildObject == null) {
			return;
		}
		ClassRoleAction action = servers.get(guildObject.getId().asString());
		if (action == null) {
			return;
		}
		ChannelListing channelObj = action.getChannelListing(event.getMessage().getChannelId());
		if (channelObj == null) {
			return;
		}
		Message message = event.getMessage();
		logger.info("Processing message text {}} in channel {}", message.getContent().toLowerCase(), channelObj.id);
		action.processMessage(message, channelObj);
	}

	@Override
	protected ClassRoleAction clone() throws CloneNotSupportedException {
		return (ClassRoleAction)super.clone();
	}

	public static void processReactionAddEvent(ReactionAddEvent event) {
		Guild guildObject = event.getGuild().block();
		if (guildObject == null) {
			return;
		}
		ClassRoleAction action = servers.get(guildObject.getId().asString());
		if (action == null) {
			return;
		}
		ChannelListing channelObj = action.getChannelListing(event.getChannelId());
		if (channelObj == null) {
			return;
		}
		Message message = event.getMessage().block();
		Member member = event.getMember().orElse(null);
		ReactionEmoji emoji = event.getEmoji();
		if (message == null || member == null) {
			return;
		}
		logger.info(String.format("Processing add reaction by %s on message %s", member.getId(), message.getId()));
		action.processAddReaction(message, member, emoji, channelObj);
	}

	public static void processReactionRemoveEvent(ReactionRemoveEvent event) {
		Guild guildObject = event.getGuild().block();
		if (guildObject == null) {
			return;
		}
		ClassRoleAction action = servers.get(guildObject.getId().asString());
		if (action == null) {
			return;
		}
		ChannelListing channelObj = action.getChannelListing(event.getChannelId());
		if (channelObj == null) {
			return;
		}
		Message message = event.getMessage().block();
		User user = event.getUser().block();
		ReactionEmoji emoji = event.getEmoji();
		if (message == null || user == null) {
			return;
		}
		logger.info(String.format("Processing remove reaction by %s on message %s", user.getId(), message.getId()));
		action.processRemoveReaction(message, user, emoji, channelObj);
	}

	public static void processGuildEvent(GuildCreateEvent event) {
		Guild guild = event.getGuild();
		List<ChannelListing> channels = processGuild(guild);
		if (channels == null) {
			logger.warn("Could not process guild add.");
			return;
		}
		ClassRoleAction action = new ClassRoleAction(guild, channels);
		servers.put(guild.getId().asString(), action);
		logger.info("Added guild {}", guild.getId());
	}

	public static void processPinUpdate(PinsUpdateEvent event) {
		Guild guild = event.getGuild().block();
		List<ChannelListing> channels = processGuild(guild);
		if (channels == null) {
			logger.info("Could not process new pin.");
			return;
		}
		ClassRoleAction action = new ClassRoleAction(guild, channels);
		servers.put(guild.getId().asString(), action);
		logger.info("Added/updated guild {}", guild.getId());
	}

	private static List<ChannelListing> processGuild(Guild guild) {
		TextChannel adminChannel = guild.getChannels()
				.filter(guildChannel -> guildChannel instanceof TextChannel &&
						guildChannel.getName().equals("admin")).map(guildChannel -> (TextChannel)guildChannel).blockFirst();
		if (adminChannel == null || !adminChannel.getEffectivePermissions(guild.getClient().getSelfId()).block().contains(Permission.VIEW_CHANNEL)) {
			logger.warn("No visible admin channel in guild {}", guild.getId().asString());
			return null;
		}
		Message configMessage = adminChannel.getPinnedMessages().filter(message -> message.getContent().startsWith("schoolbot_config")).blockFirst();
		if (configMessage == null) {
			logger.warn("No config message in admin channel {}", adminChannel.getId().asString());
			return null;
		}
		String[] lines = configMessage.getContent().split("\n");
		ArrayList<ChannelListing> channels = new ArrayList<>();
		for (String l : lines) {
			if (l.startsWith("schoolbot_config") || l.startsWith("#"))
				continue;
			Pattern pattern = Pattern.compile("^([a-z-]+), (true|false), (true|false), ([0-9]+), (.*), ([a-z-]+)$");
			Matcher matcher = pattern.matcher(l);
			if (matcher.find()) {
				Snowflake channelId = guild.getChannels().filter(guildChannel -> guildChannel.getName().equals(matcher.group(1))).map(GuildChannel::getId).blockFirst();
				if (channelId == null) {
					logger.warn("Channel {} does not exist", matcher.group(1));
					return null;
				}
				try {
					Pattern.compile(matcher.group(5));
				} catch (PatternSyntaxException exception) {
					logger.warn("Provided regex {} is not valid", matcher.group(5));
					return null;
				}
				boolean role = Boolean.parseBoolean(matcher.group(2)), channel = Boolean.parseBoolean(matcher.group(3));
				if (!(role || channel)) {
					logger.warn("Must want either role or channel.");
					return null;
				}
				Snowflake categoryId = guild.getChannels().filter(guildChannel -> guildChannel.getName().equals(matcher.group(6)) && guildChannel instanceof Category).map(GuildChannel::getId).blockFirst();
				if (categoryId == null && !matcher.group(6).equals("none")) {
					logger.warn("Category {} does not exist", matcher.group(6));
					return null;
				}
				ChannelListing newCh = new ChannelListing(channelId, role, channel,
						Integer.parseInt(matcher.group(4)), matcher.group(5), categoryId);
				if (channels.contains(newCh)) {
					logger.warn("Already added a channel with ID {}", channelId.asString());
					return null;
				}
				channels.add(newCh);
			} else {
				logger.warn("Invalid line {} in config message", l);
				return null;
			}
		}
		return channels;
	}
}
